<?php
/**
 * Created by PhpStorm.
 * User: Emeric PAIN
 * Date: 22/05/2019
 * Time: 11:37
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("Select date_mise_en_service, date_verification, url_pdf from epi left join verifications on epi.numero_serie = verifications.epi where numero_serie = ? ORDER BY date_verification desc limit 1");
$query->execute(array($_POST['epi']));
$infosVerif = $query->fetchAll();

ob_get_clean();
echo json_encode($infosVerif);