<?php
/**
 * Created by PhpStorm.
 * User: Emeric PAIN
 * Date: 17/04/2019
 * Time: 18:25
 */

include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

ob_get_clean();
session_start();
$success = array();
$old_password = $bdd->prepare("SELECT mot_de_passe from verificateurs where certificat = ?");
$old_password->execute(array($_SESSION['certificat']));
$result = $old_password->fetchAll();


if ($result[0]['mot_de_passe'] != hash("sha256", $_POST['old_password'])) {
    $success['success'] = false;
    echo json_encode($success);
    exit();
}

$query = $bdd->prepare("UPDATE verificateurs SET mot_de_passe = ? where certificat = ?");
try {
    $success['success'] = $query->execute(array(hash("sha256", $_POST['new_password']), $_SESSION['certificat']));
    echo json_encode($success);
} catch (Exception $exception) {
    echo json_encode(array(
        "success" => $exception));
}


