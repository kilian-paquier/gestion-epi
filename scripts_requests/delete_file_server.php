<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 22/05/2019
 * Time: 10:16
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("SELECT url_file from suppression_epi");

$result = $query->execute();
$elementToDelete = $query->fetchAll();


foreach ($elementToDelete as $item) {
    $file = dirname(__DIR__, 1) . str_replace("gestion-epi.org", "", $item[0]);
    if (unlink($file)) {
        echo "$item[0] effacé avec succès\n";
    } else {
        echo "Impossible d'effacer le fichier $item[0]\n";
    }
}

$query = $bdd->prepare("DELETE FROM suppression_epi");
$result = $query->execute();
