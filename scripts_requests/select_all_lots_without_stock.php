<?php
/**
 * Created by PhpStorm.
 * User: Does
 * Date: 25/05/2019
 * Time: 12:08
 */

include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("SELECT nom_lot, (
    (SELECT COUNT(numero_serie) 
    FROM epi e INNER JOIN verifications v ON (e.numero_serie = v.epi)
    WHERE e.lot = nom_lot AND v.etat <> 'rebut')
    + 
    (SELECT COUNT(e2.numero_serie)
	FROM epi e2
	WHERE e2.lot = nom_lot AND e2.numero_serie NOT IN (
    	SELECT v2.epi
    	FROM verifications v2))) AS nb_epi, groupe 
FROM lot 
HAVING nom_lot <> 'Stock' 
ORDER BY groupe DESC;");
$query->execute();
$lot = array();
$i = 0;
$lot = $query->fetchAll();
ob_get_clean(); //pour clean echo
echo json_encode($lot);
