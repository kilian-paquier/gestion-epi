<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 11/05/2019
 * Time: 15:53
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

session_start();

$result = array();

if ($_SESSION["permission"] != 0) {
    ob_get_clean();
    $result["success"] = "Vous n'avez pas les droits nécessaires";
    echo json_encode($result);
    exit();
}

if ($_POST['password'] === "") {
    $query = $bdd->prepare("UPDATE verificateurs SET nom = ?, prenom = ?, mail = ? where certificat = ?");

    try {
        $result["success"] = $query->execute(array(
            $_POST['nom'],
            $_POST['prenom'],
            $_POST['mail'],
            $_POST['certificat']
        ));

        if ($result['success'] && ($_SESSION['certificat'] == $_POST['certificat'])) {
            $_SESSION['mail'] = $_POST['mail'];
            $_SESSION['nom'] = $_POST['nom'];
            $_SESSION['prenom'] = $_POST['prenom'];
        }

        ob_get_clean();
        echo json_encode($result);
    } catch (Exception $exception) {
        ob_get_clean();
        $result["success"] = $exception->getMessage();
        echo json_encode($result);
    }
} else {
    $query = $bdd->prepare("UPDATE verificateurs SET nom = ?, prenom = ?, mail = ?, mot_de_passe = ? where certificat = ?");

    try {
        $result["success"] = $query->execute(array(
            $_POST['nom'],
            $_POST['prenom'],
            $_POST['mail'],
            hash("sha256", $_POST['password']),
            $_POST['certificat']
        ));

        if ($result['success'] && ($_SESSION['certificat'] == $_POST['certificat'])) {
            $_SESSION['mail'] = $_POST['mail'];
            $_SESSION['nom'] = $_POST['nom'];
            $_SESSION['prenom'] = $_POST['prenom'];
        }

        ob_get_clean();
        echo json_encode($result);
    } catch (Exception $exception) {
        ob_get_clean();
        $result["success"] = $exception->getMessage();
        echo json_encode($result);
    }
}