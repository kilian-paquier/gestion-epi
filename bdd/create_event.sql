CREATE EVENT delete_epi ON SCHEDULE EVERY 1 MONTH STARTS '2019-05-25 01:00:00' DO
  BEGIN
    CREATE TEMPORARY TABLE depasse_epi
    SELECT epi
    FROM verifications v
    WHERE v.date_verification <> ''
      AND DATEDIFF(STR_TO_DATE(CONCAT('01-', v.date_verification), '%d-%m-%Y'), CURDATE()) <= -5 * 365
      AND v.etat = 'rebut';

    INSERT INTO depasse_epi
    SELECT numero_serie
    FROM epi e
    WHERE e.date_fin_de_vie <> ''
      AND DATEDIFF(STR_TO_DATE(CONCAT('01-', e.date_fin_de_vie), '%d-%m-%Y'), CURDATE()) <= -5 * 365;

    INSERT INTO suppression_epi (url_file)
    SELECT url_pdf
    FROM verifications v
    where v.epi IN (SELECT epi FROM depasse_epi)
      AND url_pdf IS NOT NULL
      AND url_pdf <> '';

    INSERT INTO suppression_epi (url_file)
    SELECT image
    FROM controles c
    WHERE epi_verification IN (select epi FROM depasse_epi)
      AND image IS NOT NULL
      AND image <> '';

    INSERT INTO suppression_epi (url_file)
    SELECT image
    FROM epi e
    WHERE numero_serie IN (select epi FROM depasse_epi)
      AND image IS NOT NULL
      AND image <> '';

    DELETE
    FROM epi
    where numero_serie IN (SELECT numero_serie from depasse_epi);

    DROP TEMPORARY TABLE depasse_epi;
  END
