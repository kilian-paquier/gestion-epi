/**
 * Fonction de suppression d'un compte
 */
function deleteAccount() {
    event.preventDefault();

    /**
     * On réalise un appel ajax afin de supprimer le compte
     */
    $.post(
        '/scripts_requests/delete_verificateur.php',
        {
            certificat: $("#certificat").val()
        },
        function (data) {
            try {
                let success = JSON.parse(data);

                if (success["success"] === true) {
                    displayInfoMessage("Le compte vérificateur a bien été supprimé");
                    window.location.href = "/profil?suppression=success"
                } else if (success["success"] === "Administrateur") {
                    displayErrorMessage("Vous ne pouvez pas supprimer un compte administrateur");
                }
                else {
                    displayErrorMessage("Impossible de supprimer le compte");
                }
            } catch (e) {
                console.log(e);
                displayErrorMessage("Erreur lors de la suppression du compte");
            }
        },
        'text'
    )
}

/**
 * Fonction de modification d'un compte
 * @returns {boolean}
 */
function editAccount() {
    console.log("Entrée modification compte");
    let nom = $("#nom").val();
    let prenom = $("#prenom").val();
    let mail = $("#mail").val();
    let password = $("#password").val();

    /**
     * On valide l'email
     */
    if (!validateEmail(mail)) {
        console.log("Mail non valide");
        displayErrorMessage("L'email saisi est invalide");
        return false;
    }

    /**
     * On vérifie que les champs ne soient pas vides
     */
    if (nom === "" || prenom === "" || mail === "") {
        console.log("Champs non tous remplit");
        displayErrorMessage("Veuillez remplir tous les champs");
        return false;
    }

    /**
     * On réalise un appel de modification du compte
     */
    $.post(
        '/scripts_requests/update_account.php',
        {
            nom: nom,
            prenom: prenom,
            mail: mail,
            password: password,
            certificat: $("#certificat").val()
        },
        function (data) {
            console.log(data);
            try {
                let success = JSON.parse(data);
                if (success['success'] === true) {
                    console.log("Modification du compte succès");
                    displayInfoMessage("Le compte vérificateur a bien été modifié");
                    window.location.href = "/profil?modification=success";
                } else if (success['success'].includes("Duplicata")) {
                    displayErrorMessage("Le mail est déjà utilisé par un autre compte");
                } else {
                    displayErrorMessage("Impossible de modifier le compte vérificateur");
                }
            } catch (e) {
                console.log(e);
                displayErrorMessage("Erreur lors de la modification du compte");
            }
        },
        'text'
    )
}

$(document).ready(function () {
    let url = new URL(window.location);

    /**
     * On récupère les informations du compte lié au certificat
     */
    $.get(
        '/scripts_requests/select_verificateur.php',
        {
            certificat: url.searchParams.get("certificat"),
        },
        function (data) {
            try {
                let compte = JSON.parse(data);

                $("#mail").val(compte[0]["mail"]);
                $("#certificat").val(compte[0]["certificat"]);
                $("#nom").val(compte[0]["nom"]);
                $("#prenom").val(compte[0]["prenom"]);

            } catch (e) {
                console.log(e);
                displayErrorMessage("Impossible de charger les données du compte");
            }
        },
        'text'
    );

    /**
     * On vérifie que les deux mots de passe soient identiques lorsqu'une touche est saisie
     */
    $("#password_2").on('keyup', function () {
        let password = $("#password").val();
        let passwordRepeated = $("#password_2").val();
        samePassword(password, passwordRepeated);
    });

    /**
     * On vérifie que les deux mots de passe soient identiques lorsqu'une touche est saisie
     */
    $("#password").on("keyup", function () {
        let password = $("#password").val();
        let passwordRepeated = $("#password_2").val();
        samePassword(password, passwordRepeated);
    });

    /**
     * On vérifie la validité du mail
     */
    $("#mail").on("keyup", function () {
        let validEmail = validateEmail($("#mail").val());
        if (!validEmail) {
            displayErrorMessage("L'email saisi est invalide");
        } else {
            hideMessage();
        }
    });

    /**
     * On définit l'action de suppression du compte
     */
    $("#deleteLink").on("click", function () {
        deleteAccount();
    });

    $("#deleteButton").on("click", function () {
       event.preventDefault();
    });

    /**
     * On définit l'action de modification du compte
     */
    $("#modifButton").on("click", function () {
        event.preventDefault();

        let password = $("#password").val();
        let passwordRepeated = $("#password_2").val();

        /**
         * On vérifie que les mots de passe soient les mêmes
         */
        if (!samePassword(password, passwordRepeated))
            return false;

        /**
         * On va d'abord demander confirmation à l'administrateur qu'il veut modifier le mot de passe du vérificateur
         */
        if (password !== "") {
            $("#modalConfirmModify").modal("show");

            $("#modifyLink").on("click", function () {
                editAccount();
            })
        } else {
            console.log("modification sans mot de passe");
            editAccount();
        }
    });

    let oldValuePrenom = "";
    $("#prenom").on("keyup", function () {
        let prenom = $("#prenom").val();
        if (prenom !== "" && oldValuePrenom !== prenom) {
            let firstLetter = prenom.charAt(0).toUpperCase();
            let string = prenom.slice(1);

            oldValuePrenom = firstLetter + string;

            $("#prenom").val(firstLetter + string);
        }
    });

    let oldValueNom = "";
    $("#nom").on("keyup", function () {
        let prenom = $("#nom").val();
        if (prenom !== "" && oldValueNom !== prenom) {
            let firstLetter = prenom.charAt(0).toUpperCase();
            let string = prenom.slice(1);

            oldValueNom = firstLetter + string;

            $("#nom").val(firstLetter + string);
        }
    });
});