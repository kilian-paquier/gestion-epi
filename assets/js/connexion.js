$(document).ready(function () {
    $("#username").on('keyup', function () {
        textChanged();
    });

    $("#password").on('keyup', function () {
        runConnect(event);
    });

    $("#modalActivate").on('click', function () {
        modalOpened();
    });

    $("#email").on('keyup', function () {
        runPassword(event);
    });

    $("#getPassword").on('click', function () {
        passwordForgotten();
    });

    $("#connexionButton").on('click', function () {
        connexion();
    });
});

/**
 * Action déclenchée lors d'une touche saisie sur le champs, on lance la connexion si la touch est "Entrée"
 * @param event
 */
function runConnect(event) {
    if (event.keyCode === 13) {
        connexion();
    } else
        $(".success").html("");
}

/**
 * Fonction remettant le texte de warning ou success à vide lors de la saisie d'une touche sur un champs
 */
function textChanged() {
    $(".success").html("");
}

/**
 * Action déclenchée lorsque le modal de mot de passe oublié est ouvert
 */
function modalOpened() {
    $(".success").html("");
    $(".passwordForgotten").html("");
    $("#email").val("");
}

/**
 * Action de connexion
 * On vérifie si le mot de passe ou le login ont bien été saisi
 * On appelle un script PHP afin de checker si les identifiants sont bons et de créer la session pour l'utilisateur.
 * @returns {boolean}
 */
function connexion() {
    event.preventDefault();
    let login = $("#username").val();
    let password = $('#password').val();

    if (login === "" || password === "") {
        $('.success').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Veuillez remplir tous les champs de connexion");
        return false;
    }

    //password = sha256(password);
    $.post(
        '/scripts_requests/request_connexion.php',
        {
            username: login,
            password: password
        },
        function (data) {
            try {
                let session = JSON.parse(data);
                if (session['certificat'] === login) {
                    $('.success').addClass("alert mt-2 text-info mb-0 pb-0 font-weight-bold").removeClass("text-danger").html("Connexion réussie, vous allez être redirigé vers la page principale");
                    window.location.href = "/index?connexion=success";
                } else
                    $('.success').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Certificat vérificateur ou mot de passe invalide");
            } catch (e) {
                $('.success').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Erreur de connexion à la base de données");
                console.log(e);
            }
        },
        'text'
    );
}

/**
 * Action déclenchée sur un champs pour récupérer le mot de passe
 * On vérifie d'abord si l'email est bien un email
 * On appelle ensuite la fonction de récupération de mot de passe si la touche est "Entrée"
 * @param event
 */
function runPassword(event) {
    let mail = $("#email").val();
    if (!validateEmail(mail)) {
        $(".passwordForgotten").addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Email invalide");
    } else if (validateEmail(mail)) {
        if (event.keyCode === 13) {
            passwordForgotten()
        } else {
            $(".passwordForgotten").html("");
        }
    }
}

/**
 * Fonction de validation d'un mail en temps que mail
 * @param mail
 * @returns {boolean}
 */
function validateEmail(mail) {
    return /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(mail);
}

/**
 * Fonction de récupération de mot de passe
 * On vérifie si le mail et le certificat sont bien saisis
 * On valide l'email en temps qu'email
 * On appelle le script PHP qui va checker si le certificat et le mail correspondent bien à un compte donné
 * On appelle le script PHP qui va envoyer le mail avec un mot de passe provisoire
 * @returns {boolean}
 */
function passwordForgotten() {
    event.preventDefault();
    let mail = $("#email").val();
    let certificat = $("#certificat").val();

    if (mail === "" || certificat === "") {
        $('.passwordForgotten').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Veuillez renseigner votre mail et votre certificat vérificateur");
        return false;
    }

    if (!validateEmail(mail)) {
        $('.passwordForgotten').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Ceci n'est pas un mail");
        return false;
    }

    $.post(
        '/scripts_requests/request_password_forgotten.php',
        {
            mail: mail,
            certificat: certificat
        },
        function (data) {
            try {
                let session = JSON.parse(data);
                if (session['mail'] === mail) {
                    let password = Math.random().toString(36).slice(-8);
                    $.post(
                        '/scripts_requests/send_mail_password_forgotten.php',
                        {
                            mail: mail,
                            password: password,
                        },
                        function (data) {
                            try {
                                let success = JSON.parse(data);
                                if (success['success'] === true && success['mail'] === true) {
                                    $('.passwordForgotten').addClass("alert mt-2 text-info mb-0 pb-0 font-weight-bold").removeClass("text-danger").html("Un mail avec votre mot de passe provisoire va vous être envoyé");
                                    window.location.reload();
                                } else {
                                    $('.passwordForgotten').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Une erreur est survenue lors de l'envoi du mail avec votre mot de passe provisoire, veuillez réessayer plus tard");
                                }
                            } catch (e) {
                                $('.passwordForgotten').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Erreur de connexion à la base de données");
                            }
                        },
                        'text'
                    );
                } else
                    $('.passwordForgotten').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("L'email et le certificat donnés ne correspondent à aucun compte vérificateur");
            } catch (e) {
                $('.passwordForgotten').addClass("alert mt-2 text-danger mb-0 pb-0 font-weight-bold").removeClass("text-info").html("Erreur de connexion à la base de données");
                console.log(e);
            }
        },
    );
}