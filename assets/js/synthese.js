$(document).ready(function () {
    $('#table-service').DataTable({
        "ajax": {
            "url": "/scripts_requests/service_table.php",
            "dataSrc": "",
            "type": 'GET',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "responsive": true,
        "language": {
            "emptyTable": "Aucune donnée à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ données affichées",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table ...",
            "processing": "Recherche de données ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ données)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas de données correspondantes à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        buttons: {
            dom: {
                button: {
                    tag: 'button',
                    className: ''
                }
            },
            buttons: [{
                extend: 'pdf',
                className: 'btn special-color text-white mr-2',
                text: 'Tableau de synthèse PDF',
                extension: '.pdf',
                action: function (e, dt, node, config) {
                    exportTableToPDF(e, dt, node, config);
                }
            }]
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });

    $('#table-quarantaine').DataTable({
        "ajax": {
            "url": "/scripts_requests/quarantaine_table.php",
            "dataSrc": "",
            "type": 'GET',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "language": {
            "emptyTable": "Aucune donnée à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ données affichées",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table ...",
            "processing": "Recherche de données ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ données)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas de données correspondantes à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        buttons: {
            dom: {
                button: {
                    tag: 'button',
                    className: ''
                }
            },
            buttons: [{
                extend: 'pdf',
                className: 'btn special-color text-white mr-2',
                text: 'Tableau de synthèse PDF',
                extension: '.pdf'
            }]
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });

    $('#table-findevie').DataTable({
        "ajax": {
            "url": "/scripts_requests/fin_de_vie_table.php",
            "dataSrc": "",
            "type": 'GET',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "language": {
            "emptyTable": "Aucune donnée à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ données affichées",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table ...",
            "processing": "Recherche de données ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ données)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas de données correspondantes à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        buttons: {
            dom: {
                button: {
                    tag: 'button',
                    className: ''
                }
            },
            buttons: [{
                extend: 'pdf',
                className: 'btn special-color text-white mr-2',
                text: 'Tableau de synthèse PDF',
                extension: '.pdf'
            }]
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });

    $('#table-rebut').DataTable({
        "ajax": {
            "url": "/scripts_requests/rebut_table.php",
            "dataSrc": "",
            "type": 'GET',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "language": {
            "emptyTable": "Aucune donnée à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ données affichées",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table ...",
            "processing": "Recherche de données ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ données)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas de données correspondantes à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        buttons: {
            dom: {
                button: {
                    tag: 'button',
                    className: ''
                }
            },
            buttons: [{
                extend: 'pdf',
                className: 'btn special-color text-white mr-2',
                text: 'Tableau de synthèse PDF',
                extension: '.pdf'
            }]
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });

    $('#table-stock').DataTable({
        "ajax": {
            "url": "/scripts_requests/stock_table.php",
            "dataSrc": "",
            "type": 'GET',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "language": {
            "emptyTable": "Aucune donnée à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ données affichées",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table ...",
            "processing": "Recherche de données ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ données)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas de données correspondantes à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        buttons: {
            dom: {
                button: {
                    tag: 'button',
                    className: ''
                }
            },
            buttons: [{
                extend: 'pdf',
                className: 'btn special-color text-white mr-2',
                text: 'Tableau de synthèse PDF',
                extension: '.pdf'
            }]
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });
});

/**
 * Fonction d'exportation du fichier PDF du tableau de synthèse
 * @param e
 * @param dt
 * @param node
 * @param config
 */
function exportTableToPDF(e, dt, node, config) {

}