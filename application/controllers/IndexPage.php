<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 23/04/2019
 * Time: 17:47
 */

class IndexPage extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        require('./scripts_requests/verify_session.php');
        $this->load->view('php/index');
    }
}