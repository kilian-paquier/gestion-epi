<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 13/05/2019
 * Time: 12:11
 */

class CreateAccount extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        require('./scripts_requests/verify_session.php');
        require('./scripts_requests/verify_admin.php');
        $this->load->view('php/create-account');
    }
}