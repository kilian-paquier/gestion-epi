<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 16/05/2019
 * Time: 11:57
 */

class Custom404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->helper('url');
        $this->output->set_status_header('404');
        $this->load->view('errors/html/error_404');
    }
}