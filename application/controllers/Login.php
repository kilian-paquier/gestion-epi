<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 13/05/2019
 * Time: 20:22
 */

class Login extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        require('./scripts_requests/session_exists.php');
        $this->load->view('/php/login');
    }
}