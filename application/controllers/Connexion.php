<?php
/**
 * Created by PhpStorm.
 * User: does
 * Date: 06/03/19
 * Time: 10:42
 */

namespace controler\connexion;

use Exception;
use \PDO;


class Connexion
{
    private $bdd;

    private static $instance = NULL;

    public function __construct($util = 'root', $mdp = '', $db = 'epi_bdd')
    {
        $serveur = 'localhost';
        try {
            $this->bdd = new PDO('mysql:host=' . $serveur . ';dbname=' . $db, $util, $mdp, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            $this->bdd->exec("SET CHARACTER SET utf8");
        } catch (Exception $e) { /* Affichage d'une erreur si la connexion nest pas bonne */
            die('Erreur : ' . $e->getMessage());
        }

    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self('root', '');
        }
        return self::$instance;
    }

    /**
     * @return PDO
     */
    public function getBdd()
    {
        return $this->bdd;
    }
}