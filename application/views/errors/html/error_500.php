<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>500 Internal Server Error</title>
    <?php include "$_SERVER[DOCUMENT_ROOT]/application/views/php/head.php";?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/accueil.css">
    <style>
        .error-template {padding: 40px 15px;text-align: center;}
        .error-actions {margin-top:15px;margin-bottom:15px;}
    </style>
</head>
<body>
<div class="container-fluid mt-5 pt-5">
    <div class="container mt-5 pt-5">
        <div class="row mx-5">
            <div class="col-md-12">
                <div class="error-template" style="background-color: #f3f3f3">
                    <h1 class="bold">500 Erreur interne du serveur</h1>
                    <div class="error-details">Une erreur interne au serveur est survenue !</div>
                    <div class="error-actions"><a href="/index" class="btn btn-primary btn-lg">Retourner à la page principale </a></div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
</html>