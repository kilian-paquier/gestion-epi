<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Tableau de synthèse</title>

    <?php include("head.php"); ?>

    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

</head>

<?php include("nav.php"); ?>

<body>
<div class="container pt-5 animated fadeIn mt-5 mb-5 bg-light">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tableau des EPI en service</h3>
                    </div>
                    <div class="border border-light pr-5 pl-5 pb-3">
                        <table class="table table-hover table-responsive-lg table-striped w-100 display text-nowrap"
                               id="table-service">
                            <thead>
                            <tr>
                                <th>Type d'epi</th>
                                <th>Numéro de série</th>
                                <th>État</th>
                                <th>Marque</th>
                                <th>Identification du lot</th>
                                <th>Date de fin de vie</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tableau des EPI en stock</h3>
                    </div>
                    <div class="border border-light pr-5 pl-5 pb-3">
                        <table class="table table-hover table-responsive-lg table-striped w-100 display text-nowrap"
                               id="table-stock">
                            <thead>
                            <tr>
                                <th>Type d'epi</th>
                                <th>Numéro de série</th>
                                <th>État</th>
                                <th>Marque</th>
                                <th>Date de fin de vie</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tableau des EPI en quarantaine</h3>
                    </div>
                    <div class="border border-light pr-5 pl-5 pb-3">
                        <table class="table table-hover table-responsive-lg table-striped w-100 display text-nowrap"
                               id="table-quarantaine">
                            <thead>
                            <tr>
                                <th>Type d'epi</th>
                                <th>Numéro de série</th>
                                <th>État</th>
                                <th>Marque</th>
                                <th>Identification du lot</th>
                                <th>Date de fin de vie</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tableau des EPI au rebut</h3>
                    </div>
                    <div class="border border-light pr-5 pl-5 pb-3">
                        <table class="table table-hover table-responsive-md table-striped w-100 display text-nowrap"
                               id="table-rebut">
                            <thead>
                            <tr>
                                <th>Type d'epi</th>
                                <th>Numéro de série</th>
                                <th>État</th>
                                <th>Marque</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tableau des EPI en fin de vie</h3>
                    </div>
                    <div class="border border-light pr-5 pl-5 pb-3">
                        <table class="table table-hover table-responsive-lg table-striped w-100 display text-nowrap"
                               id="table-findevie">
                            <thead>
                            <tr>
                                <th>Type d'epi</th>
                                <th>Numéro de série</th>
                                <th>État</th>
                                <th>Marque</th>
                                <th>Identification du lot</th>
                                <th>Date de fin de vie</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/synthese.js"></script>
</html>