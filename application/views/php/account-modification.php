<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Modification de vérificateur</title>

    <?php include("head.php"); ?>

</head>

<?php include("nav.php"); ?>

<body>

<div class="container bg-light pt-5 animated fadeIn mt-5 mb-5">
    <div class="row pb-2">
        <div class="col-6 col-md-6">
            <h3 class="panel-title">Modification de compte vérificateur</h3>
        </div>
    </div>
    <form class="border border-light pr-5 pl-5 pb-3 needs-validation" novalidate id="creation_compte_form">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="certificat">Identifiant *</label>
                <input type="text" id="certificat" class="form-control mb-4" placeholder="Certificat" disabled required>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="mail">Adresse email *</label>
                <input type="email" id="mail" class="form-control mb-4" placeholder="Adresse email" required>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="nom">Nom *</label>
                <input type="text" id="nom" class="form-control mb-4" maxlength="20" placeholder="Nom" required>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="prenom">Prénom *</label>
                <input type="text" id="prenom" class="form-control mb-4" maxlength="20" placeholder="Prenom" required>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="password">Mot de passe *</label>
                <input type="password" id="password" class="form-control mb-4" maxlength="25" placeholder="Mot de passe"
                       required
                       autocomplete="off">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="password_2">Confirmer le mot de passe *</label>
                <input type="password" id="password_2" class="form-control mb-4" maxlength="25"
                       placeholder="Confirmer le mot de passe"
                       required autocomplete="off">
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <p id="success"></p>
            </div>
        </div>

        <div class="row">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-3">
                <button class="btn special-color btn-block text-white my-4" id="modifButton">Modifier le compte
                </button>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 col-xl-3">
                <button class="btn special-color btn-block text-white my-4" data-toggle="modal"
                        data-target="#modalConfirmDelete" id="deleteButton">Supprimer le compte
                </button>
            </div>
        </div>
    </form>
</div>
</body>

<!--Modal: modalConfirmDelete-->
<div class="modal fade" id="modalConfirmDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm modal-notify modal-danger" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">Êtes vous sûr de vouloir supprimer le compte ?</p>
            </div>

            <!--Body-->
            <div class="modal-body">

                <i class="fas fa-times fa-4x animated rotateIn"></i>

            </div>

            <!--Footer-->
            <div class="modal-footer flex-center">
                <button class="btn btn-outline-danger" id="deleteLink">Oui</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Non</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: modalConfirmDelete-->

<!--Modal: modalConfirmModify-->
<div class="modal fade" id="modalConfirmModify" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm modal-notify modal-warning" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">Êtes vous sûr de vouloir modifier le mot de passe du compte ?</p>
            </div>

            <!--Body-->
            <div class="modal-body">

                <i class="fas fa-times fa-4x animated rotateIn"></i>

            </div>

            <!--Footer-->
            <div class="modal-footer flex-center">
                <button class="btn btn-outline-warning" id="modifyLink">Oui</button>
                <button type="button" class="btn btn-warning waves-effect" data-dismiss="modal">Non</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: modalConfirmModify-->

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/account_modification.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/utils.js" type="text/javascript"></script>
</html>