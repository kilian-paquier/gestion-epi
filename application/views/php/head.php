<meta charset="UTF-8" content="text/html">

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="icon" href="<?php echo base_url(); ?>assets/images/webIcon.png">
<link href="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/css/mdb.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css"
      rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.5.2/css/fileinput.min.css"
      rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/table.css">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
      integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<link href="<?php echo base_url(); ?>assets/lib/jquery-flexdatalist-2.2.4/jquery.flexdatalist.min.css" rel="stylesheet">
