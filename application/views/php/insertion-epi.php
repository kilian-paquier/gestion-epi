<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Insertion d'un EPI</title>

    <?php include("head.php"); ?>

</head>

<?php include("nav.php"); ?>

<body>

<div class="container bg-light pt-5 animated fadeIn mt-5 mb-5">
    <div class="row pb-2">
        <div class="col-8 col-md-6">
            <h3 class="panel-title">Enregistrement d'un EPI</h3>
        </div>
        <div class="col-6 col-sm-6 col-md-4 offset-md-2 col-lg-3 offset-lg-3 float-right">
            <label for="nom_groupe"></label>
            <select class="browser-default custom-select mb-4" id="nom_groupe">

            </select>
        </div>
    </div>
    <form class="border border-light pr-5 pl-5 pb-3" id="insertion_epi_form">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="epi">Type d'EPI *</label>
                <select class="browser-default custom-select mb-4" id="epi" required>

                </select>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="marque">Marque *</label>
                <input type="text" id="marqueInput" class="form-control mb-4 w-100 flexdatalistMarque"
                       placeholder="Nom de la marque" list="marque" maxlength="18" required autocomplete="">
                <datalist id="marque">

                </datalist>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="lot">Identification du lot *</label>
                <input type="text" id="lot" class="form-control mb-4 w-100 flexdatalistLot lotInsertion"
                       placeholder="Identification du lot" list="list_lots" maxlength="18" required autocomplete="">
                <datalist id="list_lots" class="">

                </datalist>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="code_couleur">Code couleur</label>
                <input type="text" id="code_couleur" class="form-control mb-4" maxlength="18" placeholder="Code couleur"
                       autocomplete="off">
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="numero_serie">Identification individuelle *</label>
                <input type="text" id="numero_serie" class="form-control mb-4" maxlength="18"
                       placeholder="Identification individuelle" required autocomplete="off">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="annee_fabrication">Date de fabrication *</label>
                <input type="text" id="annee_fabrication" class="form-control mb-4 fabricationDate"
                       placeholder="Date de fabrication" required onkeypress="event.preventDefault()"
                       autocomplete="off">
                <div class="warning_fabrication"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="date_mise_en_service">Date de mise en service *</label>
                <input type="text" id="date_mise_en_service" class="form-control mb-4 serviceDate"
                       placeholder="Date de mise en service" required onkeypress="event.preventDefault()"
                       autocomplete="off">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="fin_de_vie">Date de fin de vie théorique</label>
                <input type="text" id="fin_de_vie" class="form-control mb-4 endLifeDate"
                       placeholder="Date de fin de vie"
                       onkeypress="event.preventDefault()" autocomplete="off">
                <div class="warning_fin_de_vie"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="modele">Modèle *</label>
                <input type="text" id="modele" class="form-control mb-4" maxlength="16" placeholder="Modèle" required
                       autocomplete="off">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="image">Image de l'EPI</label>
                <div class="input-default-wrapper">
                    <input type="file" id="file-with-current" class="input-default-js" accept="image/jpeg, image/png">
                    <label class="label-for-default-js rounded mb-3 bg-white" for="file-with-current">
                        <span class="span-choose-file w-75" style="font-weight: normal;">Image de l'EPI</span>
                        <div class="float-right span-browse">Rechercher</div>
                    </label>
                </div>
            </div>
        </div>

        <div class="row" id="row_for_more">

        </div>

        <div class="row">
            <div class="col-12">
                <p id="success"></p>
            </div>
        </div>

        <button class="btn special-color btn-block text-white my-4" id="submitButton" type="submit">
            Enregistrer l'EPI
            dans l'application
        </button>
    </form>

</div>

</body>

<div class="modal fade fadeInDown" id="exampleModalPreview" tabindex="-1"
     aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content p-5">
            <div class="modal-header mt-n5">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="header animated zoomIn" align="center">
                <h3 class="panel-title">Résumé insertion de l'EPI</h3>
            </div>
            <div class="contentText animated zoomIn">
                <div class="container mt-2" id="writeInfo">

                </div>
            </div>
            <div class="footer">
                <div class="row">
                    <div class="col-12 mb-0 col-md-12 col-lg-6">
                        <button class="btn special-color btn-block text-white mt-4"
                                id="submitLink" type="submit" onclick="">
                            Confirmer l'insertion de l'EPI
                        </button>
                    </div>
                    <div class="col-12 mb-n4 col-md-12 col-lg-6">
                        <button class="btn special-color btn-block text-white mt-5 mt-sm-5 mt-md-5 mt-lg-4 mb-3 mb-lg-4"
                                id="closeButton" type="submit" data-dismiss="modal">
                            Revenir à la saisie des champs
                        </button>
                    </div>
                    <div class="col-12">
                        <p id="success"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/modules/default-file-input.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/insertion_epi.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>

<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/jquery-flexdatalist-2.2.4/jquery.flexdatalist.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
        type="text/javascript"></script>
</html>
