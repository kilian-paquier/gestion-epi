<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Changement de mot de passe</title>

    <?php include("head.php"); ?>

</head>

<?php include("nav.php"); ?>

<body>
<div class="container bg-light pt-5 animated fadeIn mt-5 mb-5">
    <div class="row pb-2">
        <div class="col-6 col-md-6">
            <h3 class="panel-title">Changement de mot de passe</h3>
        </div>
    </div>
    <form class="border border-light pr-5 pl-5 pb-3 needs-validation" novalidate id="changement_mdp_from">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="old_password">Ancien mot de passe</label>
                <input type="password" id="old_password" class="form-control mb-4" placeholder="Ancien mot de passe"
                       required>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="new_password">Nouveau mot de passe</label>
                <input type="password" id="new_password" class="form-control mb-4" placeholder="Nouveau mot de passe"
                       required>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="new_password_2">Confirmer le mot de passe</label>
                <input type="password" id="new_password_2" class="form-control mb-4"
                       placeholder="Confirmer le nouveau mot de passe" required>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <p id="success"></p>
            </div>
        </div>

        <button class="btn special-color btn-block text-white my-4" id="submitButton" type="submit" onclick="">Changer le mot de passe
        </button>
    </form>

</div>
</body>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/password_change.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/utils.js" type="text/javascript"></script>
</html>