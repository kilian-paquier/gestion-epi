<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Création de vérificateur</title>

    <?php include("head.php"); ?>

</head>

<?php include("nav.php"); ?>

<body>

<div class="container bg-light pt-5 animated fadeIn mt-5 mb-5">
    <div class="row pb-2">
        <div class="col-6 col-md-6">
            <h3 class="panel-title">Création de compte vérificateur</h3>
        </div>
    </div>
    <form class="border border-light pr-5 pl-5 pb-3 needs-validation" novalidate id="creation_compte_form">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="certificat">Identifiant *</label>
                <input type="text" id="certificat" class="form-control mb-4" maxlength="16" placeholder="Certificat"
                       required>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="mail">Adresse email *</label>
                <input type="email" id="mail" class="form-control mb-4" placeholder="Adresse email" required>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="nom">Nom *</label>
                <input type="text" id="nom" class="form-control mb-4" maxlength="20" placeholder="Nom" required>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="prenom">Prénom *</label>
                <input type="text" id="prenom" class="form-control mb-4" maxlength="20" placeholder="Prenom" required>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <label for="password">Mot de passe *</label>
                <input type="password" id="password" class="form-control mb-4" maxlength="25" placeholder="Mot de passe"
                       required>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6">
                <label for="password_2">Confirmer le mot de passe *</label>
                <input type="password" id="password_2" class="form-control mb-4" maxlength="25"
                       placeholder="Confirmer le mot de passe" required>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <p id="success"></p>
            </div>
        </div>

        <button class="btn special-color btn-block text-white my-4" id="submitButton" type="submit" onclick="">Créer le
            compte vérificateur
        </button>
    </form>

</div>


</body>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/create_account.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/utils.js" type="text/javascript"></script>
</html>